function problem1(inventory){
    if(inventory == undefined){
        return undefined;
    }
    for(let i=0;i<inventory.length;i++){
        if(inventory[i] == undefined){
            continue;
        }
        if(inventory[i].id == 33){
            return inventory[i];
        }
    }
    return undefined;
}

module.exports = problem1;