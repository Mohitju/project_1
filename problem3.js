function problem3(inventory){
    if(inventory == undefined){
        return undefined;
    }
    let car_model=[];
    for( let i=0 ; i<inventory.length ; i++){
        car_model.push(inventory[i].car_model);
    }
    car_model.sort();
    return car_model;
}

module.exports = problem3;