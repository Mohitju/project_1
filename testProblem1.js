const inventory = require("./data.js");
const problem1 = require("./problem1.js");


const result = problem1(inventory );

if(result != undefined)
console.log("Car " + result.id + " is a "  + result.car_year + " " 
    + result.car_make + " " + result.car_model) ;