function problem4(inventory){
    if(inventory == undefined){
        return undefined;
    }
    var car_year=[];
    for( let i=0 ; i<inventory.length ; i++){
        car_year.push(inventory[i].car_year);
    }
    return car_year;
}
module.exports = problem4;