const problem4 = require("./problem4.js");

function problem5(inventory){
    if(inventory == undefined){
        return undefined;
    }
    var car_year = problem4(inventory);
    var car_older=[];
    for( let i=0 ; i<car_year.length ; i++){
        if(car_year[i]<2000){
            car_older.push(car_year[i]);
        }
    }
    return car_older;
}

module.exports = problem5;
